# ДЗ 18

1. Создать отдельный репозиторий
2. Создать ветку feature/hw-18
3. Создать файлы index.html и xhr.js, подключить скрипт к странице
4. Ознакомиться с открытым api по ссылке https://reqres.in/api-docs
5. В скрипте реализовать 2 функции
5.1 Залогиниться с помощью метода https://reqres.in/api/login, в теле запроса указать JSON     
{    
&nbsp;&nbsp;&nbsp;&nbsp;"username": "george.bluth@reqres.in",    
&nbsp;&nbsp;&nbsp;&nbsp;"email": "george.bluth@reqres.in",    
&nbsp;&nbsp;&nbsp;&nbsp;"password": "george"  
} <br>
5.1.1 В заголовке (Content-Type) указать формат отправляемых данных: application/json;charset=utf-8 <br>
5.1.2 Извлечь полученный токен и сделать вызов второй функции, передав в нее токен <br>
5.2 Запросить список первых 10 пользователей с помощью метода https://reqres.in/api/users <br>
5.2.1 Отключить кэширование с помощью заголовка Cache-Control, выставить русскую локаль с помощью Accept-Language, 
указать формат отправляемых данных: 
application/json;charset=utf-8 и в заголовке Authorization передать токен полученный из первой функции <br>
5.2.3 Вывести полученный список email’ов пользователей массивом в консоль <br>
5.2.4 Распечатать в консоль состояния, которые проходит выполнение запроса с помощью события onreadystatechange <br>
6. Первая функция должна вызываться автоматически после загрузки страницы
7. Открыть Pull Request feature/hw-16 -> master
8. Отдать Pull Request менторам на проверку 
